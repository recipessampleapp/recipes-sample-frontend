let ui = {};

ui.image = (data) => {
    if(data.image !== "undefined") {
        return data.image
    }   
    else {
        return "/img/placeholder.png"
    }
 }
 

export default ui;
