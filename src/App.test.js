import React from 'reactn';
import reactDOM from 'react-dom';
import App from './App';

it('renders without crashing', () => {
  const div = document.createElement('div');
  reactDOM.render(<App />, div);
  reactDOM.unmountComponentAtNode(div);
});
