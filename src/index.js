import React, { useGlobal, setGlobal, getGlobal } from 'reactn';
import reactDOM from 'react-dom';

import App from './App';
import * as serviceWorker from './serviceWorker';
import './utils/index.js';
import config from './app.config.js'

import './index.css';
import 'antd/dist/antd.css';
import './assets/css/tachyons.min.css';
import './assets/css/style.css';

setGlobal(JSON.parse(localStorage.getItem('g')));
setGlobal({
    recipeData: {},
    editRecipt: false,
    formConfig: config.forms.insert
})
if(typeof getGlobal().userIsLoggedIn === "undefined"){
    setGlobal({
        userIsLoggedIn: false
    })
}

window.getGlobal = getGlobal;

reactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
