import React, { Component } from 'reactn';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams,
    Redirect
} from "react-router-dom";
import methods from '../../utils';
import { Icon } from 'antd'
import PageTitle from '../../components/PageTitle';
import Wrapper from '../../components/Wrapper';
const formConfig = {
    fields: [
        {
            title: "title",
            type: "text",
            label: "Title",
            description: "e,g Fig & prosciutto pizzettas",
            value: ""
        },
        {
            title: "description",
            type: "text",
            label: "Description",
            description: "e.g Impress when entertaining with these mini pizzas topped with creamy mozzarella, sweet fruit and salty Italian ham.",
            value: ""
        },
        {
            title: "image",
            type: "text",
            label: "Image",
            description: "e.g https://xxxx.com/img/recipe.png",
            value: ""
        },
        {
            title: "cuisine",
            type: "text",
            label: "Cuisine",
            description: "e.g italian",
            value: ""
        },
        {
            title: "ingredientsItems",
            type: "array",
            label: "Ingredients",
            description: "e.g garlic",
            value: [""]

        },
        {
            title: "ingredients",
            type: "array",
            label: "Prepared Ingredients",
            description: "e.g 2 garlic cloves, crushed",
            value: [""]

        },        
        {
            title: "method",
            type: "array",
            label: "Method Steps",
            description: "",
            value: [""]
        }
    ]
}
class EditRecipe extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            editing: false,
            formConfig: formConfig,
            activeField: null,
            activeIndex: 0,
            form: {},
            redirectHome: false
        }       
        console.log(props)

        // this.title = React.createRef()
        // this.description = React.createRef()
        // this.image = React.createRef()
        // this.cuisine = React.createRef()

        this.insertRecipe = this.insertRecipe.bind(this);
        this.addIngredientInput = this.addIngredientInput.bind(this);
        this.setInputValue = this.setInputValue.bind(this);
        this.setInputValueinArray = this.setInputValueinArray.bind(this);
        this.addInput = this.addInput.bind(this);
    }
    async insertRecipe(e) {
        e.preventDefault()
        console.log('EditRecipe state',this.state)
        let form = this.state.form;
        let data = {};
        for (var key in form) {
            console.log('key',key)
            if (form.hasOwnProperty(key)) {
                console.log('key value',form[key].value)
                data[key] = form[key].value
            }
        }
        let data_ = {
            data, id: this.props.match.params.id
        }
        console.log('form submit data',data)
        // let res = await methods.updateRecipe(data_);
        let res = await methods.insertRecipe(data);
        if (res.status) {
            this.setState({ redirectHome: true })
        }
    }
    addIngredientInput() {
        let inputs = this.state.form.ingredients.value;
        inputs.push("")
        this.setState({
            Ingredients: inputs
        })
    }
    addInput(type) {
        let form = this.state.form;
        let inputs = form[type].value;
        inputs.push("")
        form[type].value = inputs;
        this.setState({
            form: form
        })
    }
    setInputValueinArray(event) {
        const value = event.target.value;
        console.log('[[ set field type ]]',event.target.value)
        let form = this.state.form;
        const type = this.state.activeField;
        const index = this.state.activeIndex;
        const fields = this.state.form[type].value;
        fields[index] = value;
        form[type].value = fields;
        this.setState({
            form: form
        })
    }
    setInputValue(event) {
        const value = event.target.value;
        console.log('[[ set input value ]]', value)
        console.log('[[ state ]]', this.state)
        let form = this.state.form;
        let activeField = this.state.activeField;
        form[activeField].value = value;
        this.setState({
            form: form
        })
    }
    setFieldType(data) {
        console.log('[[ set field type ]]',data)
        const type = data.field.title;
        const index = data.index;
        this.setState({ activeField: type, activeIndex: index })
    }

   async componentDidMount() {       
        if(!JSON.parse(localStorage.getItem('editRecipe')) && this.props.match.params.id !== "undefined"){
            this.setState({redirectHome: true})
        }
        let config = { self: this }
        return await methods.initialiseEditForm(config)        
    }
    render() {
        if (this.state.redirectHome) {
            return (
                <Redirect to={"/"} />
            )
        }
        return (
            <Wrapper>
            <section id="AddRecipe" className="">
                <PageTitle title={this.state.editing ? "Edit Recipe" : "Add Recipe"} subtitle={this.state.editing ? "Edit the recipe using the form below" : "Add a new recipe using the form below."} />
                {
                    this.state.ready
                        ? <form className="form flex flex-column w-100 pv4 ph0">

                            {
                                this.state.formConfig.fields.map((field, index) => (

                                    field.type === "array"
                                        ? <div key={index} className="form-row flex flex-column w-100 pb4">
                                            <div className="form-row flex flex-column w-100 pb2">
                                                <span className="f5 fw5 black-40 pb2 ttc pb2">{field.label}</span>
                                                { 
                                                    field.value.map((item, index) => (
                                                        field.title === "method"
                                                            ?
                                                            <div key={index} className="flex flex-column pb3">
                                                                <textarea defaultValue={item} onClick={() => this.setFieldType({ field, index })} type={field.title} onChange={this.setInputValueinArray} rows={2} className="flex flex-column ph3 pv3 f5 fw5 black-60 bn bs-a br1" />
                                                            </div>
                                                            :
                                                            <div key={index} className="flex flex-column pb3">
                                                                <input defaultValue={item} onClick={() => this.setFieldType({ field, index })} type={field.title} onChange={this.setInputValueinArray} className="flex flex-column ph3 pv3 f5 fw5 black-60 bn bs-a br1" />
                                                            </div>
                                                    ))}
                                                <label className="f7 fw1 black-40 pv2  ttc">{field.description}</label>

                                            </div>
                                            <div className="flex flex-row pt0">
                                                {
                                                    <a onClick={() => this.addInput(field.title)} className="flex flex-row ph2 pv1 bg-black-20 br1 bs-a bn ">
                                                    <span className="f6 fw5 white"><Icon type="plus" className="f7 fw6 white mr2" /> Add Step </span>
                                                </a>
                                                }
                                            </div>
                                        </div>


                                        : (
                                            <div key={index} className="form-row flex flex-column w-100 pb4">
                                                <span className="f5 fw5 black-40 pb2 ttc pb2">{field.label}</span>
                                                <input defaultValue={field.defaultValue} onChange={this.setInputValue} onClick={() => this.setFieldType({ field, index })} ref={this[field.title]} className="flex flex-column ph3 pv3 f5 fw5 black-60 bn bs-a br1" />
                                                <label className="f7 fw1 black-40 pv2 ttc">{field.description}</label>
                                            </div>
                                        )

                                ))
                            }
                            <div className="form-button flex flex-row w-100 pt4">
                                <button onClick={this.insertRecipe} className="flex flex-column ph3 pv2 bg-black-30 br1 bs-a bn ">
                                    <span className="f5 fw5 white"><Icon type="plus" className="f6 fw6 white mr2" /> Add Recipe</span>
                                </button>
                            </div>
                        </form>
                        : null
                }

            </section >
            </Wrapper>
        )
    }
}

export default EditRecipe