import React, { Component } from 'reactn';
import { Link } from 'react-router-dom';
import { Skeleton, Empty, Icon } from 'antd';
import ui from '../utils/ui';

class List extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            style: "grid",
            buttonLoading: this.props.buttonLoading
        }
        this.recipeImage = this.recipeImage.bind(this)
    }
    recipeImage(recipe) {
        return ui.image(recipe)
    }
    render() {
        return(
            <section id="List" className="flex flex-column flex-row-ns flex-wrap w-100 justify-between">
                { this.props.ready
                ? this.props.recipes.length === 0
                ? <div id="NoData" className="flex flex-column pv4 ph0"><div className="flex flex-column"><span className="f4 fw4 black-20"><Icon type="info-circle" className="mr2"/> No recipes available</span></div><div className="flex flex-row pt4"><button onClick={this.props.importDemoData} className="flex flex-column ph3 pv2 bg-black-30 br1 bs-a bn pointer "><span className="f5 fw5 white">{ this.props.buttonLoading ? <Icon type="loading" className={'mr3 white'} /> : <Icon type="download" className="mr2 white "/> } Import demo data</span></button></div></div>
                : this.state.style === "grid" ? <ListGrid recipeImage={this.recipeImage} items={this.props.recipes} /> : <ListRows recipeImage={this.recipeImage} items={this.props.recipes} />
                : <Skeleton active /> }  
            </section>
        )
    }
}
export default List

function ListGrid(props) {
    return (
        props.items.map((item,index) => (
            <Link key={index} to={"/view/" + item._id} className="recipe-item flex flex-column justify-between pointer mb4">
                <div className="flex flex-column flex-auto bs-a br2 overflow-hidden">
                    <div className="flex flex-column flex-auto h4 w-100 cover bg-center" style={{backgroundImage: 'url(' + props.recipeImage(item) + ')'}}></div>
                    <div className="flex flex-column flex-auto ph3 pb3">
                        <div className="flex flex-column flex-auto pv3"><span className="f4 fw6 black-90">{item.title}</span></div>
                        <div className="flex flex-column flex-auto pb3"><span className="f6 fw5 black-60">{item.description}</span></div>
                        <div className="flex flex-row flex-auto pt2"><span className="f7 fw5 black-60 ph2 pv0 ba b--black-05- bg-black-20 white br1">{item.cuisine}</span></div>
                    </div>
                </div>
            </Link>
        ))
    )
}
function ListRows(props) {
    return (
        props.items.map((item,index) => (
            <Link key={index} to={"/view/" + item._id} className="recipe-item flex flex-column justify-between pointer mb4">
                <div className="flex flex-column flex-auto bs-a br2 overflow-hidden">
                    <div className="flex flex-column flex-auto h4 w-100 cover bg-center" style={{backgroundImage: 'url(' + props.recipeImage(item) + ')'}}></div>
                    <div className="flex flex-column flex-auto ph3 pb3">
                        <div className="flex flex-column flex-auto pv3"><span className="f4 fw6 black-90">{item.title}</span></div>
                        <div className="flex flex-column flex-auto pb3"><span className="f6 fw5 black-60">{item.description}</span></div>
                        <div className="flex flex-row flex-auto pt2"><span className="f7 fw5 black-60 ph2 pv0 ba b--black-05- bg-black-20 white br1">{item.cuisine}</span></div>
                    </div>
                </div>
            </Link>
        ))
    )
}
