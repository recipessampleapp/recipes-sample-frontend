import React from 'reactn';

export default ({ title, subtitle }) => 
<section id="PageTitle" className={ " flex flex-column w-100 w-60-ns relative ph0 pb4 " }>
  <h1 className={" sans-serif f3 fw6 black-90 mb0 pb3"}>
   {title}
  </h1>
  <h5 className={" sans-serif f5 fw5 black-20 mb0 "}>
   {subtitle}
  </h5>
</section>;